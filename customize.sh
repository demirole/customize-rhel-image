#!/bin/bash

_self_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

image=$1
if [[ ! -f $image ]] ; then
    echo "Usage: `basename $0` /path/to/rhel/image"
    exit 254
fi

echo ">>> find a suitable ssh-key..."
for keyname in id_ed25519.pub id_rsa.pub
do
    for path in . $HOME/.ssh
    do
	if [[ -f "$path/$keyname" ]] ; then
	    key="$path/$keyname"
	    echo ">>> Found $key!"
	    break 2
	fi
    done
done

if [[ -z $key ]] ; then
    echo "no public ssh key found, bailling out..."
    exit 255
fi

cat <<EOF > /tmp/firstboot.sh
#!/bin/bash
groupadd -f docker
useradd -m -g users -G wheel,docker -d /home/$USER -s /bin/bash $USER
mkdir /home/$USER/.ssh
cat /$(basename $key) > /home/$USER/.ssh/authorized_keys
rm /$(basename $key)
chmod 700 /home/$USER/.ssh
chmod 600 /home/$USER/.ssh/authorized_keys
chown -R ${USER}: /home/$USER
sed -i 's/^%wheel.*/%wheel       ALL=(ALL)       NOPASSWD: ALL/' /etc/sudoers
sed -i 's/^#UseDNS.*/UseDNS no/' /etc/ssh/sshd_config
systemctl restart sshd
EOF

cat <<EOF > /tmp/commands.lst
root-password password:root
uninstall cloud-init
upload $key:/$(basename $key)
firstboot /tmp/firstboot.sh
EOF


LIBGUESTFS_BACKEND=direct
virt-customize -a $image --commands-from-file /tmp/commands.lst
rm -f /tmp/commands.lst /tmp/firstboot.sh
exit 0






